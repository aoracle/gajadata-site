<?php
/**
 * Template for the post and page titles.
 *
 * @package Author
 * @since Author 1.0
 */
?>
	<?php
		if ( is_category() ) :
			echo "<div class='sub-title'><h1>";
				printf( __( 'Category / ', 'author' ) ); single_cat_title();
			echo "</h1></div>"; 

		elseif ( is_tag() ) :
			echo "<div class='sub-title'><h1>";
				printf( __( 'Tag / ', 'author' ) ); single_tag_title();
			echo "</h1></div>";

		elseif ( is_author() ) :
			the_post();
			echo "<div class='sub-title'><h1>";
				printf( __( 'Author / %s', 'author' ), '' . get_the_author() . '' );
			echo "</h1></div>";
			rewind_posts();

		elseif ( is_day() ) :
			echo "<div class='sub-title'><h1>";
				printf( __( 'Day / %s', 'author' ), '<span>' . get_the_date() . '</span>' );
			echo "</h1></div>";

		elseif ( is_month() ) :
			echo "<div class='sub-title'><h1>";
				printf( __( 'Month / %s', 'author' ), '<span>' . get_the_date( 'F Y' ) . '</span>' );
			echo "</h1></div>";

		elseif ( is_year() ) :
			echo "<div class='sub-title'><h1>";
				printf( __( 'Year / %s', 'author' ), '<span>' . get_the_date( 'Y' ) . '</span>' );
			echo "</h1></div>";

		elseif ( is_404() ) :
			echo "<div class='sub-title'><h1>";
				_e( '404 / Page Not Found', 'author' );
			echo "</h1></div>";

		elseif ( is_search() ) :
			echo "<div class='sub-title'><h1>";
				printf( __( 'Search Results for: %s', 'author' ), '<span>' . get_search_query() . '</span>' );
			echo "</h1></div>";

		endif;
	?>