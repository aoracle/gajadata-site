<?php
/**
 * Author functions, scripts and styles.
 *
 * @package Author
 * @since Author 1.0
 */


if ( ! function_exists( 'author_setup' ) ):
/**
 * Sets up theme defaults and registers support for various WordPress features.
 * @since Author 1.0
 */
function author_setup() {

	/* Add custom post styles */
	require( get_template_directory() . '/includes/editor/add-styles.php' );

	/* Add Customizer settings */
	require( get_template_directory() . '/customizer.php' );

	/* Add theme update class */
	require( get_template_directory() . '/includes/updates/EDD_SL_Setup.php' );

	/* Add default posts and comments RSS feed links to head */
	add_theme_support( 'automatic-feed-links' );

	/* Add editor styles */
	add_editor_style();

	/* Enable support for Post Thumbnails */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 150, 150, true ); // Default Thumb
	add_image_size( 'large-image', 9999, 9999, false ); // Large Post Image

	/* Custom Background Support */
	add_theme_support( 'custom-background' );

	/* Register Menu */
	register_nav_menus( array(
		'main' 		=> __( 'Main Menu', 'author' ),
		'secondary' => __( 'Secondary', 'author' ),
		'footer' 	=> __( 'Footer Menu', 'author' ),
		'custom' 	=> __( 'Custom Menu', 'author' )
	) );

	/* Make theme available for translation */
	load_theme_textdomain( 'author', get_template_directory() . '/languages' );

	/* Add gallery format and custom gallery support */
	add_theme_support( 'post-formats', array( 'gallery' ) );
	add_theme_support( 'okay_themes_gallery_support' );

}
endif; // author_setup
add_action( 'after_setup_theme', 'author_setup' );


/* Enqueue scripts and styles */
function author_scripts() {

	//Main Stylesheet
	wp_enqueue_style( 'author-author-style', get_stylesheet_uri() );

	//Media Queries CSS
	wp_enqueue_style( 'media-queries-css', get_template_directory_uri() . "/media-queries.css", array(), '0.1', 'screen' );

	//Font Awesome
	wp_enqueue_style( 'author-fontawesome-css', get_template_directory_uri() . "/includes/fonts/fontawesome/font-awesome.css", array(), '0.1', 'screen' );

	//Flexslider CSS
	wp_enqueue_style( 'author-flexslider-css', get_template_directory_uri() . "/includes/js/flexslider/flexslider.css", array(), '0.1', 'screen' );

	//Google Font
	wp_enqueue_style( 'google_varela', 'http://fonts.googleapis.com/css?family=Roboto:300,400' );

	//Enqueue jQuery
	wp_enqueue_script( 'jquery' );

	//Custom Scripts
	wp_enqueue_script( 'author-custom-js', get_template_directory_uri() . '/includes/js/custom/custom.js', array(), '20130731', true );

	//Fitvids
	wp_enqueue_script( 'author-fitvids-js', get_template_directory_uri() . '/includes/js/fitvid/jquery.fitvids.js', array(), '20130731', true );

	//Flexslider
	wp_enqueue_script( 'author-flexslider-js', get_template_directory_uri() . '/includes/js/flexslider/jquery.flexslider.js', array(), '20130731', true );

	//HTML5 IE Shiv
	wp_enqueue_script( 'author-htmlshiv-js', get_template_directory_uri() . '/includes/js/html5/html5shiv.js', array(), '20130731', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}

}
add_action( 'wp_enqueue_scripts', 'author_scripts' );


/* Set the content width */
if ( ! isset( $content_width ) )
	$content_width = 690; /* pixels */


/* Register sidebars */
if ( function_exists( 'register_sidebars' ) )
register_sidebar( array(
	'name' 			=> __( 'Sidebar', 'author' ),
	'description' 	=> __( 'Widgets in this area will be shown on the sidebar of all pages.', 'author' ),
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' 	=> '</div>'
) );

register_sidebar( array(
	'name' 			=> __( 'Footer Icons', 'author' ),
	'description' 	=> __( 'This widget area is for the social media icons widget in the footer.', 'author' ),
	'before_widget' => '<div id="%1$s" class="widget %2$s">',
	'after_widget' 	=> '</div>'
) );


/* If excerpt slider is enabled, ignore sticky posts since we feature them in the slider */
function author_ignore_sticky( $query ) {
	if ( is_home() && $query->is_main_query() && get_option( 'author_customizer_slider_disable') == 'enable' )
        $query->set('ignore_sticky_posts', true);
}
add_action( 'pre_get_posts', 'author_ignore_sticky' );


/* Pagination conditional */
function author_page_has_nav() {
	global $wp_query;
	return ($wp_query->max_num_pages > 1);
}


/* Add Customizer CSS To Header */
function author_customizer_css() {
    ?>
	<style type="text/css">
		.post-content a {
			color: <?php echo get_theme_mod( 'author_customizer_accent', '#55BEE6' ) ;?>;
		}

		.ribbon {
			border-color: <?php echo get_theme_mod( 'author_customizer_accent', '#55BEE6' ) ;?> <?php echo get_theme_mod( 'author_customizer_accent', '#55BEE6' ) ;?> white;
		}
		
		<?php echo get_theme_mod( 'author_customizer_css', '' ); ?>
	</style>
    <?php
}
add_action( 'wp_head', 'author_customizer_css' );


/* Custom Comment Output */
function author_comments( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment; ?>
	<li <?php comment_class('clearfix'); ?> id="li-comment-<?php comment_ID() ?>">
		
		<div class="comment-block" id="comment-<?php comment_ID(); ?>">
			<div class="comment-info">	
				<div class="comment-author vcard clearfix">
					<?php echo get_avatar( $comment->comment_author_email, 75 ); ?>
					
					<div class="comment-meta commentmetadata">
						<?php printf(__('<cite class="fn">%s</cite>', 'north'), get_comment_author_link()) ?>
					</div>
				</div>
			</div><!-- comment info -->
			
			<div class="comment-text">
				<?php comment_text() ?>
				
				<div class="comment-bottom">
					<p class="reply">
						<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ) ?>
					</p>
					<?php edit_comment_link(__('Edit', 'north'),' ', '' ) ?>
					<a class="comment-time" href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s at %2$s', 'north'), get_comment_date(),  get_comment_time()) ?></a>
				</div>
			</div><!-- comment text -->
		
			<?php if ($comment->comment_approved == '0') : ?>
				<em class="comment-awaiting-moderation"><?php _e('Your comment is awaiting moderation.', 'north') ?></em>
			<?php endif; ?>    
		</div>
<?php
}

function author_cancel_comment_reply_button( $html, $link, $text ) {
    $style = isset($_GET['replytocom']) ? '' : ' style="display:none;"';
    $button = '<div id="cancel-comment-reply-link"' . $style . '>';
    return $button . '<i class="icon-remove-sign"></i> </div>';
}
 
add_action( 'cancel_comment_reply_link', 'author_cancel_comment_reply_button', 10, 3 );
 

/* Check for Okay Toolkit Notice */
if ( !function_exists( 'okaysocial_init' ) ) {

	function okay_toolkit_notice() {
	    global $current_user ;
	    $user_id = $current_user->ID;
	    
	    $adminurl = admin_url('plugin-install.php?tab=plugin-information&plugin=okay-toolkit&TB_iframe=true&width=640&height=589');
	    
	    if ( ! get_user_meta( $user_id, 'okay_toolkit_ignore_notice' ) ) { 
	        echo '<div class="updated"><p>';

	        echo 'This theme requires the Okay Toolkit! Install it to extend the features of your theme. ';
	        
	        echo '<a class="thickbox onclick" href=" ' . $adminurl . ' ">Install Now</a> | ';
	        
	        printf(__('<a href="%1$s">Hide Notice</a>'), '?okay_toolkit_nag_ignore=0');
	        
	        echo "</p></div>";
	    }
	}
	add_action( 'admin_notices', 'okay_toolkit_notice' );
	
	function okay_toolkit_nag_ignore() {
	    global $current_user;
	        $user_id = $current_user->ID;
	        /* If user clicks to ignore the notice, add that to their user meta */
	        if ( isset($_GET['okay_toolkit_nag_ignore']) && '0' == $_GET['okay_toolkit_nag_ignore'] ) {
	             add_user_meta( $user_id, 'okay_toolkit_ignore_notice', 'true', true );
	    }
	}
	add_action( 'admin_init', 'okay_toolkit_nag_ignore' );

}