<?php
/**
 * Template for the sidebar and it's widgets.
 *
 * @package Author
 * @since Author 1.0
 */
?>

				<div id="sidebar">			
					<!-- load sidebar widgets -->				
					<?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Sidebar') ) : else : ?>		
					<?php endif; ?>	
				</div><!--sidebar-->
